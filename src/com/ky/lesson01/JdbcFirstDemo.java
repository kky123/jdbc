package com.ky.lesson01;

import java.sql.*;

/**
 * @author：ky
 * @date 2020-12-04 15:55
 */
public class JdbcFirstDemo {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {

        //加载驱动
        Class.forName("com.mysql.jdbc.Driver");
        String url = "jdbc:mysql://localhost:3306/crm?useUnicode=true&characterEncoding=utf8";
        String username = "root";
        String password = "123456";
        // 连接成功数据库对象 connection 代表数据库
        Connection connection = DriverManager.getConnection(url, username, password);
        //执行sql对象 Statement
        Statement statement = connection.createStatement();
        //执行sql
        String sql = "select * from sysuser";

        ResultSet resultSet = statement.executeQuery(sql);

        while (resultSet.next()) {
            System.out.println("id=" + resultSet.getObject("id"));
            System.out.println("username=" + resultSet.getObject("username"));
            System.out.println("password=" + resultSet.getObject("password"));
            System.out.println("email=" + resultSet.getObject("email"));
            System.out.println("===============================");
        }

        //释放连接
        resultSet.close();
        statement.close();
        connection.close();


    }
}
