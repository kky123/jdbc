package com.ky.lesson04;

import com.ky.lesson04.utils.JdbcUtils_DBCP;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author：ky
 * @date 2020-12-07 16:43
 */
public class TestDBCP {
    public static void main(String[] args) {
        Connection conn = null;
        PreparedStatement st = null;
        try {
            conn = JdbcUtils_DBCP.getConnection();
            //区别
            //使用占位符代替参数。
            String sql = "INSERT INTO sysuser (username,password)VALUES(?, ?)";
            st = conn.prepareStatement(sql);

            st.setObject(1, "kyyy");
            st.setObject(2, "66666");

            int i = st.executeUpdate();
            if (i>0){
                System.out.println("插入成功");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
