package com.ky.lesson04.utils;

import org.apache.commons.dbcp2.BasicDataSourceFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.util.Properties;

/**
 * @author：ky
 * @date 2020-12-07 16:35
 */
public class JdbcUtils_DBCP {

    private static DataSource dataSource;
    static {
        try {
            Properties properties = new Properties();
            properties.load(JdbcUtils_DBCP.class.getClassLoader().getResourceAsStream("dbcpconfig.properties"));
            dataSource = BasicDataSourceFactory.createDataSource(properties);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     *
     * @return
     */
    public static DataSource getDataSource() {
        return dataSource;
    }

    /**
     *
     * @return
     */
    public static Connection getConnection() {
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            return conn;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}
