package com.ky.lesson04;

import com.ky.lesson04.utils.JdbcUtils_C3P0;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author：ky
 * @date 2020-12-07 17:26
 */
public class TestC3P0 {
    public static void main(String[] args) {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            conn = JdbcUtils_C3P0.getConnection();
            String sql = "select * from sysuser where id = ?";
            st = conn.prepareStatement(sql);
            st.setInt(1,3);//传递参数

            //
            rs = st.executeQuery();

            if (rs.next()){
                System.out.println(rs.getString("username"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JdbcUtils_C3P0.release(conn,st,rs);
        }
    }
}
