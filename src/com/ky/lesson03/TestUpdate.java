package com.ky.lesson03;

import com.ky.lesson02.utils.JdbcUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author：ky
 * @date 2020-12-07 15:38
 */
public class TestUpdate {
    public static void main(String[] args) {

        Connection conn = null;
        PreparedStatement st = null;
        try {
            conn = JdbcUtil.getConnection();

            //区别
            //使用占位符代替参数。
            String sql = "update sysuser set username = ?  where id = ? ;";
            st = conn.prepareStatement(sql);

            st.setObject(1, "kyyy");
            st.setObject(2, "3");

            int i = st.executeUpdate();
            if (i>0){
                System.out.println("更新成功");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JdbcUtil.release(conn,st,null);
        }
    }
}
