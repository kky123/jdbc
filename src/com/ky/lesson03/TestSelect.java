package com.ky.lesson03;

import com.ky.lesson02.utils.JdbcUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author：ky
 * @date 2020-12-07 15:43
 */
public class TestSelect {
    public static void main(String[] args) {

        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
             conn = JdbcUtil.getConnection();
             String sql = "select * from sysuser where id = ?";
             st = conn.prepareStatement(sql);
             st.setInt(1,3);//传递参数

            //
             rs = st.executeQuery();

             if (rs.next()){
                 System.out.println(rs.getString("username"));
             }

        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JdbcUtil.release(conn,st,rs);
        }
    }
}
