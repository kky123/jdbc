package com.ky.lesson02;

import com.ky.lesson02.utils.JdbcUtil;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author：ky
 * @date 2020-12-04 17:27
 */
public class TestSelect {
    public static void main(String[] args) {
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        try {
            conn = JdbcUtil.getConnection();
            st = conn.createStatement();

            String sql = "select * from sysuser where id = 22";

            rs = st.executeQuery(sql);

            if (rs.next()){
                System.out.println(rs.getObject("username"));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            JdbcUtil.release(conn,st,rs);
        }
    }
}
