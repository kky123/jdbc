package com.ky.lesson02;

import com.ky.lesson02.utils.JdbcUtil;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author：ky
 * @date 2020-12-04 16:56
 */
public class TestInsert {

    public static void main(String[] args) {

        Connection conn=null;
        Statement st =null;
        ResultSet rs =null;
        try {
             conn = JdbcUtil.getConnection();//获取数据库连接
             st = conn.createStatement();
             String sql ="INSERT INTO sysuser (username,password)VALUES('ky', '123456')";
             int i = st.executeUpdate(sql);
             if (i>0){
                 System.out.println("插入成功");
             }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            JdbcUtil.release(conn,st,rs);
        }
    }
}
